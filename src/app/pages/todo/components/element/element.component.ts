import {Component, Input, OnInit} from '@angular/core';
import {ITodo} from "../../../../models/todo";
import {BehaviorSubject} from "rxjs";
import {TodoService} from "../../../../services/todo.service";

@Component({
  selector: 'app-element',
  templateUrl: './element.component.html'
})
export class ElementComponent implements OnInit {

  @Input() todo: any

  checked: boolean = false

  handleChecked() {
    this.todoServices.checked(this.todo)
    const stream = this.todoServices.todo$.subscribe(v => console.log(v))
    stream.unsubscribe()
  }

  handleDelete(){
    this.todoServices.delete(this.todo)
  }

  constructor(private todoServices: TodoService) {
  }


  ngOnInit(): void {
  }

}
