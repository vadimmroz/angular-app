import { Component, OnInit } from '@angular/core';
import {TodoService} from "../../services/todo.service";
import {ModalService} from "../../services/modal.service";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html'
})
export class TodoComponent implements OnInit {

  constructor(public todoService: TodoService, public modalServices: ModalService) { }

  ngOnInit(): void {
  }

  openModal(){
    this.modalServices.open()
  }

}
