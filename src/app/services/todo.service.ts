import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {ITodo} from "../models/todo";

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  todo: ITodo[] = []

  todo$ = new BehaviorSubject<ITodo[]>([])

  constructor() {
  }

  addTodo(elem: ITodo) {
    this.todo.push(elem)
    this.todo$.next(this.todo)
  }

  checked(elem: ITodo) {
    this.todo = this.todo.filter(t => {
        if (t.id === elem.id) {
          t.completed = !t.completed
        }
        return t
      }
    )
    this.todo$.next(this.todo)
  }

  delete(elem: ITodo) {
    this.todo = this.todo.filter(t=> t !== elem)
    this.todo$.next(this.todo)
  }
}
