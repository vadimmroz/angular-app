import { Injectable } from '@angular/core';
import {TodoService} from "./todo.service";
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor() { }

  modalIsVisible$ = new BehaviorSubject<boolean>(false)

  open(){
    this.modalIsVisible$.next(true)
  }
  close(){
    this.modalIsVisible$.next(false)
  }
}
