import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html'
})
export class CounterComponent implements OnInit {

  count = 0

  constructor() { }

  ngOnInit(): void {
  }

  increment(){
    this.count++
  }
  decrement(){
    this.count--
  }

}
