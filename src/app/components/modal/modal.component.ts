import {Component, OnInit} from '@angular/core';
import {TodoService} from "../../services/todo.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ModalService} from "../../services/modal.service";

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html'
})
export class ModalComponent implements OnInit {

  form = new FormGroup({
    name: new FormControl<string>("", [
      Validators.required,
      Validators.minLength(3)
    ])
  })

  constructor(private todoServices: TodoService, public modalServices: ModalService) {
  }

  ngOnInit(): void {
  }
  get name() {
    return this.form.controls.name as FormControl
  }

  submit() {
    this.todoServices.addTodo({name: this.form.value.name as string, id: Math.random(), completed: false})
    this.modalServices.close()
  }

}
