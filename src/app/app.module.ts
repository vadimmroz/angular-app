import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CounterComponent } from './components/counter/counter.component';
import { TodoComponent } from './pages/todo/todo.component';
import { ModalComponent } from './components/modal/modal.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { FocusDirective } from './directives/focus.directive';
import { ElementComponent } from './pages/todo/components/element/element.component';

@NgModule({
  declarations: [
    AppComponent,
    CounterComponent,
    TodoComponent,
    ModalComponent,
    FocusDirective,
    ElementComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
